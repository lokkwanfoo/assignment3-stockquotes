/*jslint browser: true, plusplus:true*/

describe("scenario", function () {

    it("assertion", function () {
        expect(1).toBe(1);
    });

});


describe("html tests", function () {

    var properties, methods;

    beforeEach(function () {
        var propertyName;

        app.init();

        properties = [];
        methods = [];
        for (propertyName in app) {
            if (app.hasOwnProperty(propertyName)) {
                if (app[propertyName].constructor === Function) {
                    methods.push(app[propertyName].prototype);
                } else {
                    properties.push(propertyName);
                }
            }
        }
    });

afterEach(function () {
    document.body.removeChild(document.querySelector("#container"));
});

it("Should verify that the app has properties and methods.", function () {
    expect(properties.length).not.toBe(0);
    expect(methods.length).not.toBe(0);

});

it("Should verify that the app has properties and methods.", function () {
        expect(properties.length).not.toBe(0);
        expect(methods.length).not.toBe(0);

});

it("Should verify that series is defined and is an object", function () {
        var actualValue = app.series;
        expect(actualValue).toBeDefined();
        expect(actualValue.constructor).toBe(Object);
});

it("Should verify that series is defined and is an object", function () {
    var actualValue = app.series;
    expect(actualValue).toBeDefined();
    expect(actualValue.constructor).toBe(Object);
});


it("Should verify that DOM elements are created for the container and the title", function () {
    var expectedValue = 'Real Time Stockquote App';
    var actualValue = app.initHTML().querySelector("h1").innerText;

    expect(actualValue).toBe(expectedValue);

});

it("Should verify that the table has 25 rows", function () {
    if (app.settings.choice === 0) {
        var actualValue = app.showData().querySelectorAll("tr").length;
        expect(actualValue).toBe(25);
    } else {
        expect(actualValue).toBe(undefined);
    }
});


it("Should verify that only letters and numbers remain", function () {
    var actualValue = app.createValidCSSNameFromCompanyName("HGF>&^#%%$#@");
    var expectedValue = "HGF";
    expect(actualValue).toBe(expectedValue);
});

it("Should verify retrieving realtime data cannot be tested", function () {
    if (typeof app.getRealTimeData === 'function') { 
      var actualValue = app.getRealTimeData();
    expect(actualValue).not.toBeDefined();
    }
    
});

it("It should verify that ranges including mid and max", function () {
    var input = 100, range = 5, hitMin = 0, hitMax = 0, i, r;

    for (i = 0; i < 1000; i++) {
        r = app.rnd(input, range);
        if (r === input - range) {
            hitMin++;
        } else if (r === input + range) {
            hitMax++;
        }
    }

    console.log('hitMin', hitMin);
    console.log('hitMax', hitMax);

    expect(hitMin).toBeGreaterThan(0);
    expect(hitMax).toBeGreaterThan(0);

});

});