/*global io, app, data */
(function () {
    "use strict";
    window.app = {

        series: {},
        socket: io("http://server7.tezzt.nl:1333"),
        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php",
            dataPoints: 10,
            choice: 2 //0 = static test data, 1 = websockets, 2 = Ajax request, 3 = generating test data
        },

        rnd: function (input, range) {
            var max = input + range,
                min = input - range;
            return Math.floor(
                Math.random() * (max - min + 1)
            ) + min;
        },

        getDataFromAjax: function () {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", app.settings.ajaxUrl);
            xhr.addEventListener("load", app.retrieveRows);
            xhr.send();
        },

        retrieveRows: function (e) {
            var str, obj, rows;
            str = e.target.responseText;
            obj = JSON.parse(str);
            rows = obj.query.results.row;
            app.parseData(rows);
        },

        getDate: function (date) {
            var datetime, month, day, year;

            month = date.getMonth()
                .toString();
            day = date.getDay()
                .toString();
            year = date.getFullYear()
                .toString();

            if (month.length < 2) {
                month = "0" + month;
            }
            if (day.length < 2) {
                day = "0" + day;
            }

            datetime = month + "/" + day + "/" + year;


            return datetime;
        },

        getTime: function (date) {
            var hours, minutes, strTime;
            hours = date.getHours()
                .toFixed();
            minutes = date.getMinutes()
                .toString();

            if (hours.length > 1) {
                if (minutes.length < 2) {
                    minutes = "0" + minutes.toString();
                }
                strTime = (24 - hours)
                    .toString() + ":" + minutes + " pm";
            } else {
                strTime = hours.toString() + ":" + minutes + " am";
            }
            return strTime;
        },

        generateTestData: function () {
            var company, quote, newQuote;

            for (company in app.series) {
                if (app.series.hasOwnProperty(company)) {
                    quote = app.series[company][0];
                    newQuote = Object.create(quote);
                    newQuote.col1 = Math.random() * 100; // new value, should be calculated with rnd
                    newQuote.col2 = new Date(); // new date
                    newQuote.col3 = new Date(); // new time, including am, pm
                    newQuote.col4 = -1 + Math.floor(Math.random() * 3); // difference of price value between this quote and the previous quote

                    app.series[company].unshift(newQuote);
                }
            }
        },

        getRealTimeData: function () {
            app.socket.on('stockquotes', function (data) {
                app.parseData(data.query.results.row);
            });
        },

        parseData: function (rows) {
            var i, company;

            // Iterate over the rows and add to series
            for (i = 0; i < rows.length; i++) {
                company = rows[i].col0;

                // Check if array for company exist in series
                if (app.series[company] !== undefined) {
                    app.series[company].unshift(rows[i]);
                } else {
                    // company does not yet exist
                    app.series[company] = [rows[i]];
                }
                if (app.series.length > app.settings.dataPoints) {
                    app.series.shift();
                }
            }

        },

        createValidCSSNameFromCompanyName: function (str) {
            return str.replace(/\W/g, "");
        },

        showData: function () {
            // return value is a dom
            var table, company, row, quote, cell, propertyName, propertyValue;

            // Create table
            table = document.createElement("table");

            // Create header


            // Create rows
            for (company in app.series) {
                if (app.series.hasOwnProperty(company)) {
                    quote = app.series[company][0];
                    row = document.createElement("tr");

                    // Create cells
                    table.appendChild(row);

                    // Iterate over quote to create cells
                    for (propertyName in quote) {
                        if (quote.hasOwnProperty(propertyName)) {
                            propertyValue = quote[propertyName];
                            cell = document.createElement("td");
                            cell.innerText = propertyValue;
                            //cell.class = "winner";
                            row.appendChild(cell);
                        }
                    }

                    if (quote.col4 < 0) {
                        row.className = "loser";
                    } else if (quote.col4 > 0) {
                        row.className = "winner";
                    }
                }
            }

            return table;

        },


        loop: function () {
            var table;

            switch (app.settings.choice) {
            case 0:
                app.parseData(data.query.results.row);
                break;
            case 1:
                app.getRealTimeData();
                break;
            case 2:
                app.getDataFromAjax();
                break;
            case 3:
                app.generateTestData();
                break;
            }

            // Remove old table
            document.querySelector("#container").removeChild(document.querySelector("table"));

            table = app.showData();
            app.container.appendChild(table);

            setTimeout(app.loop, app.settings.refresh);
        },

        initHTML: function () {
            var container, h1Node;

            // Create container
            container = document.createElement("div");
            container.id = "container";

            app.container = container;


            // Create title of application
            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";

            app.container.appendChild(h1Node);

            return app.container;

        },

        init: function () {

            var container, table;

            // Add HTML to page
            container = app.initHTML();
            document.querySelector("body").appendChild(container);

            switch (app.settings.choice) {
            case 0:
                app.parseData(data.query.results.row);
                break;
            case 1:
                app.getRealTimeData();
                break;
            case 2:
                app.getDataFromAjax();
                break;
            case 3:
                app.generateTestData();
                break;
            }

            table = app.showData();
            app.container.appendChild(table);

            app.loop();
        }

    };
}());